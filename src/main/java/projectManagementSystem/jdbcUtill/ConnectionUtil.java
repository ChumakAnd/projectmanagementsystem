package projectManagementSystem.jdbcUtill;

import java.sql.*;

/**
 * Created by Андрей on 11.02.2017.
 */
public class ConnectionUtil {

    public static final String JDBC_DRIVER = "org.postgresql.Driver";
    public static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/HomeWork_1";
    public static final String USER = "postgres";
    public static final String PASSWORD = "1234";


    public static Connection connection = null;
    public static Statement statement = null;
    public static PreparedStatement preparedStatement = null;

    public static void getConnection() throws SQLException, ClassNotFoundException {
        Class.forName(JDBC_DRIVER);
        connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
    }

    public static void closeConnection() throws SQLException {
        connection.close();
    }

    public static void closePreparedStatement(){
        if(preparedStatement != null){
            try{
                preparedStatement.close();
            } catch (SQLException e){
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }



}
