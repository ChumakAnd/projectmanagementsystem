package projectManagementSystem.controller.jdbcController;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import projectManagementSystem.dao.jdbc.JdbcCustomerDaoImpl;
import projectManagementSystem.model.Customer;

/**
 * Created by Андрей on 16.02.2017.
 */
public class JdbcCustomerController {
    private JdbcCustomerDaoImpl jdbcCustomerDao = new JdbcCustomerDaoImpl();

    @Transactional(propagation = Propagation.REQUIRED)
    public void getCustomerById (int id) {
        System.out.println(jdbcCustomerDao.getById(id));
    }

    public void saveCustomer(Customer customer) {
        jdbcCustomerDao.save(customer);
        System.out.println("Customer " + customer.getName() + " saved successfully");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void showAllCustomers() {
        jdbcCustomerDao.readAll().forEach(System.out::println);
    }

    public void updateCustomer (Customer customer) {
        jdbcCustomerDao.update(customer);
        System.out.println("Customer " + customer.getName() + " updated successfully");
    }

    public void deleteCustomer(Customer customer) {
        jdbcCustomerDao.delete(customer);
        System.out.println("Customer " + customer.getName() + " deleted successfully");
    }
}
