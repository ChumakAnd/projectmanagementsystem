package projectManagementSystem.controller.jdbcController;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import projectManagementSystem.dao.jdbc.JdbcCompanyDaoImpl;
import projectManagementSystem.model.Company;

/**
 * Created by Андрей on 16.02.2017.
 */
public class JdbcCompanyController {
    private JdbcCompanyDaoImpl companyDAO = new JdbcCompanyDaoImpl();
    public void setCompanyDAO(JdbcCompanyDaoImpl companyDAO) {
        this.companyDAO = companyDAO;
    }

    public void getCOmpanyById(int id) {
        System.out.println(companyDAO.getById(id));
    }

    public void saveCompany(Company company){
        companyDAO.save(company);
        System.out.println("Company " + company.getName() + " saved successfully");
    }

    public void showAllCompanies() {
        companyDAO.readAll().forEach(System.out::println);
    }

    public void updateCompany(Company company) {
        companyDAO.update(company);
        System.out.println("Company " + company.getName() + " updated successfully");
    }

    public void deleteCompany(Company company) {
        companyDAO.delete(company);
        System.out.println("Company " + company.getName() + " deleted successfully");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void allDevelopersInCompany (int companyId) {
        companyDAO.showAllDevsInCompany(companyId).forEach(System.out::println);
    }




}
