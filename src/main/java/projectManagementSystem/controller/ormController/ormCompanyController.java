package projectManagementSystem.controller.ormController;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import projectManagementSystem.dao.orm.OrmCompanyDaoImpl;
import projectManagementSystem.model.Company;

import java.util.List;

/**
 * Created by Андрей on 09.01.2017.
 */
public class ormCompanyController {
    private OrmCompanyDaoImpl ormCompaniesDao;

    public void setOrmCompaniesDao(OrmCompanyDaoImpl ormCompaniesDao) {
        this.ormCompaniesDao = ormCompaniesDao;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Company getCompanyById(int id){return ormCompaniesDao.getById(id);}


    @Transactional(propagation = Propagation.REQUIRED)
    public List<Company> read() {return ormCompaniesDao.readAll();}


    @Transactional(propagation = Propagation.REQUIRED)
    public void create(Company entity){
        ormCompaniesDao.save(entity);}


    @Transactional(propagation = Propagation.REQUIRED)
    public void update(Company company) {
        ormCompaniesDao.update(company);}


    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Company entity){
        ormCompaniesDao.delete(entity);}

    @Transactional(propagation = Propagation.REQUIRED)
    public void allDevelopersInCompany(int companyId){
        ormCompaniesDao.showAllDevsInCompany(companyId);}


}


