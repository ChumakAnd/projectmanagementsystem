package projectManagementSystem.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "company")
public class Company {


    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Developer> developerList;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projectList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Developer> getDeveloperList() {
        return developerList;
    }

    public void setDeveloperList(List<Developer> developerList) {
        this.developerList = developerList;
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }


    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + "'}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Company company = (Company) o;

        if (getId() != company.getId()) return false;
        if (getName() != null ? !getName().equals(company.getName()) : company.getName() != null) return false;
        if (getDeveloperList() != null ? !getDeveloperList().equals(company.getDeveloperList()) : company.getDeveloperList() != null)
            return false;
        return getProjectList() != null ? getProjectList().equals(company.getProjectList()) : company.getProjectList() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getDeveloperList() != null ? getDeveloperList().hashCode() : 0);
        result = 31 * result + (getProjectList() != null ? getProjectList().hashCode() : 0);
        return result;
    }
}
