package projectManagementSystem.dao.jdbc;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import projectManagementSystem.dao.CompanyDAO;
import projectManagementSystem.jdbcUtill.ConnectionUtil;
import projectManagementSystem.model.Company;
import projectManagementSystem.model.Developer;
import projectManagementSystem.model.Project;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static projectManagementSystem.jdbcUtill.ConnectionUtil.*;

/**
 * Created by Андрей on 10.01.2017.
 */
public class JdbcCompanyDaoImpl implements CompanyDAO {

   public JdbcCompanyDaoImpl(){
       try{
           getConnection();
       } catch (SQLException e){
           throw new RuntimeException("Connection failed " + e);
       } catch (ClassNotFoundException e){
           throw new RuntimeException("Driver not found " + e);
       }
   }

   public final static String GET_BY_ID = "SELECT * FROM company WHERE id = ?";
   public final static String INSERT = "INSERT INTO company (name) VALUES (?)";
   public final static String UPDATE = "UPDATE company SET name = ? WHERE id = ?";
   public final static String DELETE = "DELETE FROM company WHERE id = ?";
   public final static String SHOW_ALL = "SELECT * FROM company";
   public final static String SHOW_ALL_DEVELOPERS = "select * from public.developer where \"companyId\" = ?";
   public final static String SHOW_ALL_PROJECTS = "select * from public.project where \"companyId\" = ?";

    private Company company;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Company getById(int id) {
        try{preparedStatement = connection.prepareStatement(GET_BY_ID);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        company = new Company();
        if(resultSet.next()){
            company.setId(resultSet.getInt(1));
            company.setName(resultSet.getString(2));
            return company;
        } else {
            throw new SQLException("Cannot find Company with id = " + id);
        }
        } catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException("Exception occurred while connection to DB " + e);
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void save(Company entity) {
        try {preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new RuntimeException("Exception while adding new company " + e);
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Company> readAll() {
        List<Company> resultList = new ArrayList<>();

        try {
            try (Connection connection = ConnectionUtil.connection;) {
                try (Statement statement = connection.createStatement()) {

                   try(ResultSet resultSet = statement.executeQuery(SHOW_ALL)){

                    while (resultSet.next()) {

                        Company company = getCompany(resultSet);

                        resultList.add(company);}
                       return resultList;
                   }
            }
        }
    }catch (SQLException e){
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(Company company) {
        try {preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, company.getName());
            preparedStatement.setInt(2, company.getId());
        } catch (SQLException e){
                 throw new RuntimeException("Exception while update company" + e);
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Company company) {

        try {preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setInt(1, company.getId());
        } catch (SQLException e){
            throw new RuntimeException("Exception while deleting company " + e);
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Developer> showAllDevsInCompany(int companyId) {

        List<Developer> resultList = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(SHOW_ALL_DEVELOPERS);
            preparedStatement.setInt(1, companyId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Developer dev = new Developer();
                dev.setId(resultSet.getInt("id"));
                dev.setFirstName(resultSet.getString("firstName"));
                dev.setSecondName(resultSet.getString("secondName"));
                dev.setSalary(resultSet.getInt("salary"));
                dev.setCompany(company);
                resultList.add(dev);
            }

            return resultList;
        } catch (SQLException e){
            throw new RuntimeException("Exception while getting all developers of company " + e);
        }

    }

    @Override
    public List<Project> showAllProjects(int companyId) {

        List<Project> resultList = new ArrayList<>();
        try {preparedStatement = connection.prepareStatement(SHOW_ALL_PROJECTS);
            preparedStatement.setInt(1, companyId);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Project project = new Project();
                project.setId(resultSet.getInt("id"));
                project.setName(resultSet.getString("name"));
                resultList.add(project);

            }
            return resultList;

        } catch (SQLException e){
            throw new RuntimeException("Exception while getting all projects of company " + e);
        }

    }



    private Company getCompany(ResultSet resultSet) throws SQLException {
        Company company = new Company();
        company.setId(resultSet.getInt("id"));
        company.setName(resultSet.getString("name"));

        return company;
    }


}
