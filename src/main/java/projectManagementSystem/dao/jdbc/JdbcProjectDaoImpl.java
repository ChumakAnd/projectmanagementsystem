package projectManagementSystem.dao.jdbc;

import projectManagementSystem.dao.ProjectDAO;
import projectManagementSystem.jdbcUtill.ConnectionUtil;
import projectManagementSystem.model.Customer;
import projectManagementSystem.model.Developer;
import projectManagementSystem.model.Project;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static projectManagementSystem.jdbcUtill.ConnectionUtil.*;

/**
 * Created by Андрей on 14.02.2017.
 */
public class JdbcProjectDaoImpl implements ProjectDAO {

    public JdbcProjectDaoImpl(){
        try{
            getConnection();
        } catch (SQLException e){
            throw new RuntimeException("Connection failed " + e);
        } catch (ClassNotFoundException e){
            throw new RuntimeException("Driver not found " + e);
        }
    }

    public final static String GET_BY_ID = "SELECT * FROM project WHERE id = ?";
    public final static String INSERT = "INSERT INTO project VALUES (?, ?, ?, ?, ?)";
    public final static String UPDATE = "UPDATE project SET name = ?, \"customerId\" = ?, \"companyId\" = ?, cost = ? WHERE id = ?";
    public final static String DELETE = "DELETE FROM project WHERE id = ?";
    public final static String SHOW_ALL = "SELECT * FROM project";
    public final static String SHOW_ALL_DEVS = "select developer.id, developer.\"firstName\", developer.\"secondName\" FROM developer " +
            "INNER JOIN project ON developer.\"projectId\" = project.id WHERE developer.\"projectId\" = ?";
    public final static String SHOW_CUSTOMER = "select customer.id, customer.name\n" +
            "FROM customer INNER JOIN project ON customer.id = project.\"customerId\"\n" +
            "WHERE project.id = ?";


    private Project project;
    private List<Project> projectList;

    @Override
    public Project getById(int id) {
        try {
            preparedStatement = connection.prepareStatement(GET_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            project = new Project();
            if(resultSet.next()){
               project = this.getProject(resultSet);
                return project;
            } else {
                throw new SQLException("Cannot find project with id = " + id);
            }
        } catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException("Exception occurred while connection to DB " + e);
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    public void save(Project entity) {
        try {preparedStatement = connection.prepareStatement(INSERT);

        preparedStatement.setInt(1,entity.getId());
        preparedStatement.setString(2, entity.getName());
        preparedStatement.setInt(3, entity.getCustomerId());
        preparedStatement.setInt(4, entity.getCompanyId());
        preparedStatement.setInt(5, entity.getCost());
        preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new RuntimeException("Exception while adding new project " + e);
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    public List<Project> readAll() {
        projectList = new ArrayList<>();
        try {
            try (Connection connection = ConnectionUtil.connection;) {
                try (Statement statement = connection.createStatement()) {

                    try(ResultSet resultSet = statement.executeQuery(SHOW_ALL)){

                        while (resultSet.next()) {
                            project = getProject(resultSet);
                            projectList.add(project);}
                        return projectList;
                    }
                }
            }
        }catch (SQLException e){
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public List<Developer> devsOnProject(int projectId) {
        List<Developer> resultList = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(SHOW_ALL_DEVS);
            preparedStatement.setInt(1, projectId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Developer dev = JdbcDevelopersDaoImpl.getDeveloper(resultSet);
                resultList.add(dev);
            }
            return resultList;
        } catch (SQLException e){
            throw new RuntimeException("Exception while getting all developers of company " + e);
        }
    }

    @Override
    public void delete(Project entity) {

        try {preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setInt(1, entity.getId());
        } catch (SQLException e){
            throw new RuntimeException("Exception while deleting project " + e);
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    public Customer customer(int projectId) {
        try{preparedStatement = connection.prepareStatement(SHOW_CUSTOMER);
            preparedStatement.setInt(1, projectId);
            ResultSet resultSet = preparedStatement.executeQuery();
            Customer customer;
            if(resultSet.next()){
                customer = JdbcCustomerDaoImpl.getCustomer(resultSet);
                return customer;
            } else {
                throw new SQLException("Cannot find project with id = " + projectId);
            }
        } catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException("Exception occurred while connection to DB " + e);
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    public void update(Project entity) {
        try {preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getCustomerId());
            preparedStatement.setInt(3, entity.getCompanyId());
            preparedStatement.setInt(4, entity.getCost());
            preparedStatement.setInt(5, entity.getId());
        } catch (SQLException e){
            throw new RuntimeException("Exception while update project" + e);
        } finally {
            closePreparedStatement();
        }
    }

    private Project  getProject(ResultSet resultSet) throws SQLException {
        Project project = new Project();
        project.setId(resultSet.getInt("id"));
        project.setName(resultSet.getString("name"));
        project.setCompanyId(resultSet.getInt("customerId"));
        project.setCustomerId(resultSet.getInt("companyId"));
        project.setCost(resultSet.getInt("cost"));


        return project;
    }
}
