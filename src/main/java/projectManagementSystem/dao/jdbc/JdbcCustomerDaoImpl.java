package projectManagementSystem.dao.jdbc;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import projectManagementSystem.dao.GenericDAO;
import projectManagementSystem.jdbcUtill.ConnectionUtil;
import projectManagementSystem.model.Customer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static projectManagementSystem.jdbcUtill.ConnectionUtil.*;

/**
 * Created by Андрей on 10.01.2017.
 */
public class JdbcCustomerDaoImpl implements GenericDAO<Customer> {

    public JdbcCustomerDaoImpl() {
        try{
            getConnection();
        } catch (SQLException e){
            throw new RuntimeException("Connection failed " + e);
        } catch (ClassNotFoundException e){
            throw new RuntimeException("Driver not found " + e);
        }
    }

    public final static String GET_BY_ID = "SELECT * FROM customer WHERE id = ?";
    public final static String INSERT = "INSERT INTO customer (name) VALUES (?)";
    public final static String UPDATE = "UPDATE customer SET name = ? WHERE id = ?";
    public final static String DELETE = "DELETE FROM customer WHERE id = ?";
    public final static String SHOW_ALL = "SELECT * FROM customer";


    private Customer customer;
    private List<Customer> customerList;



    @Override
    public Customer getById(int id){
        try{preparedStatement = connection.prepareStatement(GET_BY_ID);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        customer = new Customer();
        if(resultSet.next()){
            customer.setId(resultSet.getInt(1));
            customer.setName(resultSet.getString(2));
            return customer;
        } else {
            throw new SQLException("Cannot find Customer with id = " + id);
        }
    } catch (SQLException e){
        e.printStackTrace();
        throw new RuntimeException("Exception occurred while connection to DB " + e);
    } finally {
        closePreparedStatement();
    }
}


    @Override
    public void save(Customer entity) {
        try {preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new RuntimeException("Exception while adding new Customer " + e);
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Customer> readAll(){
        customerList = new ArrayList<>();
        try {
        try (Connection connection = ConnectionUtil.connection;) {
            try (Statement statement = connection.createStatement()) {

                try(ResultSet resultSet = statement.executeQuery(SHOW_ALL)){

                    while (resultSet.next()) {

                        Customer customer = getCustomer(resultSet);

                        customerList.add(customer);}
                    return customerList;
                }
            }
        }
    }catch (SQLException e){
        throw new RuntimeException(e.getMessage(), e);
    }
    }

    @Override
    public void update(Customer entity) {
        try {preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getId());
        } catch (SQLException e){
            throw new RuntimeException("Exception while update Customer" + e);
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    public void delete(Customer entity) {

        try {preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setInt(1, entity.getId());
        } catch (SQLException e){
            throw new RuntimeException("Exception while deleting Customer " + e);
        } finally {
            closePreparedStatement();
        }

    }

    public static Customer getCustomer(ResultSet resultSet) throws SQLException {
        Customer customer = new Customer();
        customer.setId(resultSet.getInt("id"));
        customer.setName(resultSet.getString("name"));

        return customer;
    }

}
