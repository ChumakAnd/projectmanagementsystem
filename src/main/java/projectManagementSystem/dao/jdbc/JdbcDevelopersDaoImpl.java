package projectManagementSystem.dao.jdbc;

import projectManagementSystem.dao.DeveloperDAO;
import projectManagementSystem.jdbcUtill.ConnectionUtil;
import projectManagementSystem.model.Company;
import projectManagementSystem.model.Developer;
import projectManagementSystem.model.Project;
import projectManagementSystem.model.Skill;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static projectManagementSystem.jdbcUtill.ConnectionUtil.*;

/**
 * Created by Андрей on 12.02.2017.
 */
public class JdbcDevelopersDaoImpl implements DeveloperDAO {

    public JdbcDevelopersDaoImpl(){
        try{
            getConnection();
        } catch (SQLException e){
            throw new RuntimeException("Connection failed " + e);
        } catch (ClassNotFoundException e){
            throw new RuntimeException("Driver not found " + e);
        }
    }

    public final static String GET_BY_ID = "SELECT * FROM developer WHERE id = ?";
    public final static String INSERT = "INSERT INTO developer (\"firstName\", \"secondName\", \"salary\") VALUES (?, ?, ?)";
    public final static String UPDATE = "UPDATE developer SET \"firstName\" = ?, \"secondName\" = ?, \"salary\" = ? WHERE developer.id = ?";
    public final static String DELETE = "DELETE FROM developer WHERE developer.id = ?";
    public final static String SHOW_ALL = "SELECT * FROM developer";
    public final static String SHOW_DEV_SKILLS = "SELECT name FROM skill INNER JOIN developer_skills ON skill.id = developer_skills.\"skillsId\" WHERE \"developerId\" = ?";
    public final static String SHOW_DEV_COMPANY = "select company.id, company.name as company FROM company INNER JOIN developer ON company.id = developer.\"companyId\" WHERE developer.id = ?";
    public final static String SHOW_PROJECT = "select project.id, project.name FROM project INNER JOIN developer ON project.id = developer.\"projectId\" WHERE developer.id = ?";

    private List<Developer> developerList;
    private Developer developer;


    @Override
    public Developer getById(int id) {
        try{preparedStatement = connection.prepareStatement(GET_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            developer = new Developer();
            if(resultSet.next()){
                developer.setId(resultSet.getInt(1));
                developer.setFirstName(resultSet.getString(2));
                developer.setSecondName(resultSet.getString(3));
                developer.setSalary(resultSet.getInt(6));
                return developer;
            } else {
                throw new SQLException("Cannot find Company with id = " + id);
            }
        } catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException("Exception occurred while connection to DB " + e);
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    public void save(Developer entity) {
        try {preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getSecondName());
            preparedStatement.setInt(3, entity.getSalary());

            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new RuntimeException("Exception while adding new company " + e);
        } finally {
            closePreparedStatement();
        }
    }

    @Override
    public List<Developer> readAll(){
       developerList = new ArrayList<>();

        try {
        try (Connection connection = ConnectionUtil.connection;) {
            try (Statement statement = connection.createStatement()) {
                try(ResultSet resultSet = statement.executeQuery(SHOW_ALL)){

                    while (resultSet.next()) {
                        developer = getDeveloper(resultSet);

                        developerList.add(developer);}
                    return developerList;
                }
            }
        }
    }catch (SQLException e){
        throw new RuntimeException(e.getMessage(), e);
    }
}


    @Override
    public void delete(Developer entity) {
        try {preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setInt(1, entity.getId());
        } catch (SQLException e){
            throw new RuntimeException("Exception while deleting Developer " + e);
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    public List<Skill> showDevelopersSkills(int developersId) {

        List<Skill> resultList = new ArrayList<>();
        try { preparedStatement = connection.prepareStatement(SHOW_DEV_SKILLS);
        preparedStatement.setInt(1, developersId);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Skill skill = new Skill();
            skill.setName(resultSet.getString("name"));
            resultList.add(skill);
        }
            return null;
        } catch (SQLException e){
            throw new RuntimeException("Exception while getting developer's skills " + e);
        }

    }

    @Override
    public void update(Developer entity) {

        try {preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getSecondName());
            preparedStatement.setInt(3, entity.getSalary());
            preparedStatement.setInt(4, entity.getId());
        } catch (SQLException e){
            throw new RuntimeException("Exception while update company" + e);
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    public void showProject(int developersId) {
        Project project = new Project();
        try { preparedStatement = connection.prepareStatement(SHOW_PROJECT);
            preparedStatement.setInt(1, developersId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                project.setName(resultSet.getString("name"));
                project.setId(resultSet.getInt("id"));
            }
            System.out.println("Developer with Id " + developersId + " works on Project with Id " + project.getId() + " and name " + project.getName());
        } catch (SQLException e){
            throw new RuntimeException("Exception while getting developer's company " + e);
        }
    }

    @Override
    public Company showCompany(int developersId) {

        List<Company> resultList = new ArrayList<>();
        try { preparedStatement = connection.prepareStatement(SHOW_DEV_COMPANY);
            preparedStatement.setInt(1, developersId);
            ResultSet resultSet = preparedStatement.executeQuery();
            Company company = new Company();

            while (resultSet.next()) {
                company.setName(resultSet.getString("company"));
                company.setId(resultSet.getInt("id"));
                resultList.add(company);
            }
            return company;
        } catch (SQLException e){
            throw new RuntimeException("Exception while getting developer's company " + e);
        }
    }

    public static Developer getDeveloper(ResultSet resultSet) throws SQLException {
        Developer developer = new Developer();
        developer.setId(resultSet.getInt("id"));
        developer.setFirstName(resultSet.getString("firstName"));
        developer.setSecondName(resultSet.getString("secondName"));
        developer.setSalary(resultSet.getInt("salary"));


        return developer;
    }
}
