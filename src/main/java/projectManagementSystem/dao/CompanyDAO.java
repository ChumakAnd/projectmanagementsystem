package projectManagementSystem.dao;

import org.springframework.transaction.annotation.Transactional;
import projectManagementSystem.model.Company;
import projectManagementSystem.model.Project;

import java.util.List;

/**
 * Created by Андрей on 10.01.2017.
 */
public interface CompanyDAO extends GenericDAO<Company> {

   @Transactional
   List showAllDevsInCompany(int companyId);

   @Transactional
   List<Project> showAllProjects(int companyId);


}
