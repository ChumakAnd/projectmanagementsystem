package projectManagementSystem.dao.orm;

import org.hibernate.SessionFactory;
import projectManagementSystem.dao.CompanyDAO;
import projectManagementSystem.model.Company;
import projectManagementSystem.model.Project;

import java.util.List;

/**
 * Created by Андрей on 04.02.2017.
 */
public class OrmCompanyDaoImpl implements CompanyDAO {
    private SessionFactory sessionFactory;




    @Override
    public Company getById(int id) {
        return null;
    }

    @Override
    public void save(Company entity) {
        sessionFactory.getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public List<Company> readAll() {
        return sessionFactory.getCurrentSession().createQuery("FROM Company").list();

    }

    @Override
    public void delete(Company entity) {


    }

    @Override
    public List showAllDevsInCompany(int companyId) {
        return null;
    }

    @Override
    public List<Project> showAllProjects(int companyId) {
        return null;
    }

    @Override
    public void update(Company company) {

    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
