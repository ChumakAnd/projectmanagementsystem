package projectManagementSystem.dao;

import projectManagementSystem.model.Customer;
import projectManagementSystem.model.Developer;
import projectManagementSystem.model.Project;

import java.util.List;

/**
 * Created by Андрей on 10.01.2017.
 */
public interface ProjectDAO extends GenericDAO<Project> {

    List<Developer> devsOnProject(int projectId);
    Customer customer (int projectId);
}
