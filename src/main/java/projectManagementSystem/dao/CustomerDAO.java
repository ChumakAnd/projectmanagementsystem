package projectManagementSystem.dao;

import org.springframework.transaction.annotation.Transactional;
import projectManagementSystem.model.Customer;
import projectManagementSystem.model.Project;

import java.util.List;

/**
 * Created by Андрей on 10.01.2017.
 */
public interface CustomerDAO extends GenericDAO<Customer> {

    @Transactional
    List<Project> showProjects (int customerId);

}
