package projectManagementSystem.dao;

import org.springframework.transaction.annotation.Transactional;
import projectManagementSystem.model.Company;
import projectManagementSystem.model.Developer;
import projectManagementSystem.model.Skill;

import java.util.List;


public interface DeveloperDAO extends GenericDAO<Developer> {

    @Transactional
    List<Skill> showDevelopersSkills(int developersId);

    @Transactional
    void showProject (int developersId);

    @Transactional
    Company showCompany(int developersId);
}
