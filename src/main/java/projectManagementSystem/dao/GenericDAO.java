package projectManagementSystem.dao;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Андрей on 09.01.2017.
 */
public interface GenericDAO<E> {
    @Transactional
    E getById(int id);

    @Transactional
    void save(E entity);

    @Transactional
    List<E> readAll();

    @Transactional
    void delete(E entity);

    @Transactional
    void update(E entity);

}
