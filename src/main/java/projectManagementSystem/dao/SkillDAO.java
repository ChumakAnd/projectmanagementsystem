package projectManagementSystem.dao;

import projectManagementSystem.model.Developer;
import projectManagementSystem.model.Skill;

import java.util.List;

/**
 * Created by Андрей on 10.01.2017.
 */
public interface SkillDAO extends GenericDAO<Skill> {

    List<Developer> devsWithSkill(int skillId);
}
