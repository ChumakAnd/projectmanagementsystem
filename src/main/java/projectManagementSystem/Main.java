package projectManagementSystem;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import projectManagementSystem.controller.jdbcController.JdbcCompanyController;
import projectManagementSystem.controller.jdbcController.JdbcCustomerController;

/**
 * Created by Андрей on 09.01.2017.
 */
public class Main {

    private JdbcCompanyController jdbcCompanyController = new JdbcCompanyController();
    private JdbcCustomerController jdbcCustomerController = new JdbcCustomerController();



    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        Main main = context.getBean("projectManagementSystem.Main", Main.class);
        main.start();


    }

    public void start() {
        /*jdbcCompanyController.allDevelopersInCompany(2);
        jdbcCompanyController.showAllCompanies();*/
        System.out.println("=============================");
        jdbcCustomerController.showAllCustomers();

    }

    public void setJdbcCompanyController(JdbcCompanyController jdbcCompanyController) {
        this.jdbcCompanyController = jdbcCompanyController;
    }

    public void setJdbcCustomerController(JdbcCustomerController jdbcCustomerController) {
        this.jdbcCustomerController = jdbcCustomerController;
    }
}


